from rest_framework import serializers
from comments.models import Comment


class CommentSerializer(serializers.ModelSerializer):

    reply_count = serializers.SerializerMethodField()

    class Meta:
        model = Comment
        fields = ['id', 'object_id', 'parent', 'content', 'timestamp', 'reply_count']

    def get_reply_count(self, obj):
        if obj.is_parent:
            return obj.children().count()
        return 0

class CommentChildSerializer(serializers.ModelSerializer):

    class Meta:
        model = Comment
        fields = ['id', 'content', 'timestamp']

class CommentDetailSerializer(serializers.ModelSerializer):

    replies = serializers.SerializerMethodField()
    reply_count = serializers.SerializerMethodField()

    class Meta:
        model = Comment
        fields = ['id', 'object_id', 'content', 'replies', 'timestamp', 'reply_count']

    def get_replies(self, obj):
        if obj.is_parent:
            return CommentChildSerializer(obj.children(), many=True).data
        return None

    def get_reply_count(self, obj):
        if obj.is_parent:
            return obj.children().count()
        return 0
