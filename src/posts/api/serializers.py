from rest_framework import serializers
from posts.models import Post


class PostCreateUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Post
        fields = ['title', 'content', 'publish']

class PostListSerializer(serializers.ModelSerializer):

    url = serializers.HyperlinkedIdentityField(view_name='posts-api:detail', lookup_field='slug')
    user = serializers.SerializerMethodField()

    class Meta:
        model = Post
        fields = ['user', 'url', 'title', 'publish']

    def get_user(self, obj):
        return str(obj.user.username)

class PostDetailSerializer(serializers.ModelSerializer):

    url = serializers.HyperlinkedIdentityField(view_name='posts-api:detail', lookup_field='slug')
    user = serializers.SerializerMethodField()
    image = serializers.SerializerMethodField()
    html = serializers.SerializerMethodField()

    class Meta:
        model = Post
        fields = ['url', 'user', 'image', 'title', 'slug', 'content', 'markdown', 'publish']

    def get_user(self, obj):
        return str(obj.user.username)

    def get_html(self, obj):
        return str(obj.get_markdown())

    def get_image(self, obj):
        try:
            image = obj.image.url
        except:
            image = None
        return image
